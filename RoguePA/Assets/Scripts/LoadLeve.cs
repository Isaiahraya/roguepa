﻿using UnityEngine;
using System.Collections;

public class LoadLeve : MonoBehaviour {

		
    private bool playerInZone;

    public string levelToLoad;

    // Use this for initialization
    void Start()
    {
        playerInZone = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && playerInZone)
        {
            Debug.Log("keycode is down");
            Application.LoadLevel(levelToLoad);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        Debug.Log("Entered Trigger");
        if (other.name == "Player")
        {
            Debug.Log("Collided with Player");
            playerInZone = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.name == "Player")
        {
            playerInZone = false;
        }
    }
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Player : MonoBehaviour
{

    private int playerSpeed = 4;
    private int jumpHeight = 1500;
    public bool isGrounded = false;
    public Text healthText;
    public GameObject gameOverScreen;


    public Transform lowObject;
    public Transform Respawn;

    public Player player;
    public int healthPerPizza = 15;
    public int damageReceived = 15;
    private Animator animator;
    private int playerHealth = 100;


    //public Transform portal;
    //public Transform spawnpoint;

    void FixedUpdate()
    {
        isGrounded = Physics.Raycast(transform.position, -Vector3.up, 1f);
    }

    // Use this for initialization
    void Start()
    {
        animator = GetComponent<Animator>();
        healthText.text = "Health: " + playerHealth;
    }

    // Update is called once per frame
    void Update()
    {

        if (transform.position.y < lowObject.position.y)
        {
            transform.position = Respawn.position;
        }

        if (playerHealth <= 0)
        {
            gameOverScreen.SetActive (true);
            player.gameObject.SetActive (false);
        }


        //else if (transform.position.y < portal.position.y)
        {
            //transform.position = spawnpoint.position;
        }

        Vector3 theScale = transform.localScale;
        transform.localScale = theScale;
        animator.SetFloat("run", 0);
        if (Input.GetKeyDown("right") && theScale.x == -1)
        {

            theScale = transform.localScale;
            theScale.x *= -1;
            transform.localScale = theScale;
        }

        if (Input.GetKeyDown("left") && theScale.x == -1)
        {

            theScale = transform.localScale;
            theScale.x *= 1;
            transform.localScale = theScale;
        }
        if (Input.GetKey("right"))
        {
            transform.position -= Vector3.left * playerSpeed * Time.deltaTime;
            animator.SetFloat("run", 1);

        }

        if (Input.GetKey("left"))
        {

            transform.position -= Vector3.right * playerSpeed * Time.deltaTime;
            animator.SetFloat("run", 1);
        }

        if (Input.GetKeyDown("up") && isGrounded)
        {
            GetComponent<Rigidbody>().AddForce(new Vector3(0, jumpHeight, 0), ForceMode.Force);
        }
    }

    private void OnTriggerEnter(Collider objectPlayerCollidedWith)
    {
        if (objectPlayerCollidedWith.tag == "Pizza")
        {

            playerHealth += healthPerPizza;
            healthText.text = "+" + healthPerPizza + "Health\n" + "Health: " + playerHealth;
            objectPlayerCollidedWith.gameObject.SetActive(false);
        }
        if (objectPlayerCollidedWith.tag == "Robo")
        {
            playerHealth -= damageReceived;

            healthText.text = "-" + damageReceived + " Health\n" + "Health: " + playerHealth;
            Debug.Log("took damage");
            animator.SetTrigger("Player-hurt");

        }
    }
   
  
}






